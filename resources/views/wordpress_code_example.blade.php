<html>
    <body>
        <h1>Wordpress code example:</h1>
		
		<?php
		//$blogusers = get_users( array( 'fields' => array( 'display_name' ) ) );
		$blogusers= get_users( [ 'role__in' => [ 'author', 'subscriber' ] ] );
		// Array of stdClass objects.
		foreach ( $blogusers as $user ) {
		    echo '<span>' . esc_html( $user->display_name . ": ". $user->user_email) . '</span> <br />';
		}
		?>
		
    </body>
</html>